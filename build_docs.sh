#!/usr/bin/env bash
#
# build_docs.sh
# Copyright (C) 2024 Uwe Schmitt <uwe.schmitt@id.ethz.ch>
#
# Distributed under terms of the MIT license.
#

cd docs
python build_index.py | sed -e's/[[:space:]]*$//' > index.md
