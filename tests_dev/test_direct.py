#!/usr/bin/env python


import os
import tempfile
import time

import pytest

here = os.path.abspath(__file__)

from pytest_regtest import register_converter_post, register_converter_pre


@register_converter_post
def to_upper_conv(output):
    return output.upper()


@register_converter_pre
def noop(output, request):
    return output


def test_regtest(regtest, tmpdir):
    with regtest:
        print("this is expected outcome")
        print(tmpdir.join("test").strpath)
        print(tempfile.gettempdir())
        print(tempfile.mkdtemp())
        print("obj id is", hex(id(here)))


def test_regtest_all(regtest_all, tmpdir):
    import sys

    print(sys.stdout)
    print(sys.__stdout__)
    print("this is expected outcome")
    print(tmpdir.join("test").strpath)
    print(tempfile.gettempdir())
    print(tempfile.mkdtemp())
    print("obj id is", hex(id(here)))


@pytest.mark.xfail
def test_always_fail():
    assert 1 * 1 == 2


@pytest.mark.xfail(strict=True)
def test_always_fail_regtest(regtest):
    regtest.write(str(time.time()))


@pytest.mark.xfail
def test_fail_regtest(regtest):
    regtest.write("hi")


def test_always_ok():
    assert 1 * 1 == 1


def test_always_ok_regtest(regtest):
    regtest.identifier = "my_computer"
    assert 1 * 1 == 1


@pytest.mark.parametrize("a, b, c", [(1, 2, 3), ("a", "b", "ab")])
def test_with_paramertrization(a, b, c, regtest):
    print(a, b, c, file=regtest)
    assert a + b == c


@pytest.mark.parametrize("a", ["123456." * 20])
def test_with_very_long_name(regtest, a):
    print("hi", file=regtest)
    print(repr(regtest), file=regtest)


import tempfile
import pytest

@pytest.mark.xfail(strict=False)
def test_regtest_xfail(regtest_all, tmpdir):

    print("this is expected outcome")
    print(tmpdir.join("test").strpath)
    print(tempfile.gettempdir())
    print(tempfile.mkdtemp())
    print("obj id is", hex(id(tempfile)))
